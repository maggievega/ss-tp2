package ar.edu.itba.ss;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * Hello world!
 *
 */
public class App 
{


    public static void main( String[] args ) {

        final String outFile = "output.txt";

        final int iterations = 1000;
        final double chaos = 2.0;

        final int N = 300;
        final double L = 7;
        final double Rc = 1;

        final boolean WRITE_VA = true;

        final int M = (int) (L / (Rc)); //2 = 2 * 1 = 2 * MaxRadius

        final ArrayList<Particle> list = Particle.makeParticles(N, L, 0.05, 0.03);

        Space space = new Space(M, L, Rc, list);

       // System.out.println(space);

        long time = System.currentTimeMillis();

        for (int t = 0; t < iterations; t++){
            System.out.println("t = " + t);
            updatePos(space, M, L);
            HashMap<Integer, Set<Integer>> neighbours = CellIndexMethod.cellIndexMethod(space, N, true);

           // printNeighbours(neighbours, N);

            //Update heading
            updateHeading(space.getParticleList(), neighbours, chaos);

            //write file
            printDynamicFile(outFile, t, N, space.getParticleList(), WRITE_VA);

        }


       System.out.println("Running time: " + (System.currentTimeMillis() - time)/1000.0);

    }


    private static void updatePos(final Space space, final int M, final double L){

        List<Particle> toRelocate = new ArrayList<>();
        List<Particle> toRemove = new ArrayList<>();

        for (int i = 0; i < M; i++){
            for (int j = 0; j < M; j++){
                HashSet<Particle> list = space.getParticlesXY(i,j);
                for (Particle p : list){
                    double x = positiveModule(p.getX_pos() + p.getXSpeed(), L);
                    double y = positiveModule(p.getY_pos() + p.getYSpeed(), L);

                    p.setX_pos(x);
                    p.setY_pos(y);

                    int dstX = (int) (p.getX_pos() / (L / M));
                    int dstY = (int) (p.getY_pos() / (L / M));

                    if (dstX != i || dstY != j){
                        toRemove.add(p);
                        toRelocate.add(p);
                    }
                }
                space.getParticlesXY(i, j).removeAll(toRemove);
                toRemove.clear();
            }
        }
        space.placeParticles(toRelocate);
    }

    private static double positiveModule(double val, double val2){
        return val % val2 < 0 ? val % val2 + val2 : val % val2;
    }

    private static void printNeighbours(HashMap<Integer, Set<Integer>> neighbours, int N){
        for (int i = 0; i < N; i++){
            System.out.print(i);
            for (Integer integer : neighbours.get(i)){
                System.out.print("," + integer);
            }
            System.out.print("\n");
        }
    }

    private static void printDynamicFile(String filename, int t, int N, List<Particle> list, boolean write_va){
        StringBuilder sb = new StringBuilder();
        sb.append('\t').append(N).append('\n');
        sb.append('\t').append(t).append('\n');

        double xDotSum = 0;
        double yDotSum = 0;

        for (Particle p : list) {
            sb.append('\t').append(p.getId()).append('\t').append(p.getX_pos()).append('\t').append(p.getY_pos()).append('\t')
                    .append(p.getRadius()).append('\t').append(p.getHeading()).append('\n');

            if (write_va) {
                xDotSum += p.getXSpeed();
                yDotSum += p.getYSpeed();
            }
        }


        try {
            FileWriter fw = new FileWriter(filename, true);
            fw.write(sb.toString());
            fw.close();
        } catch (IOException e){
            e.printStackTrace();
        }

        if (write_va){
            try {
                FileWriter fw = new FileWriter("Va.txt", true);
                double speedMod = Math.sqrt(Math.pow(xDotSum, 2) + Math.pow(yDotSum, 2));
                double Va = speedMod / (N * 0.03);
                fw.write(t + "\t" + Va + "\n");
                fw.close();
            } catch (IOException e){
                e.printStackTrace();
            }
        }
    }

    private static void updateHeading(List<Particle> list, HashMap<Integer, Set<Integer>> set, double chaos){
        Random rand = new Random();
        ArrayList<Double> headingList = new ArrayList<>();

        for (Particle p : list) {
            double senSum = Math.sin(p.getHeading());
            double cosSum = Math.cos(p.getHeading());

            for (Integer i : set.get(p.getId())){
                Particle neighbour = list.get(i);
                senSum += Math.sin(neighbour.getHeading());
                cosSum += Math.cos(neighbour.getHeading());
            }
            //<DTheta> + random between [-chaos/2, chaos/2]
            double newHeading = Math.atan2(senSum, cosSum) + ((Math.random() * chaos) - (chaos / 2));
            newHeading = newHeading < 0 ? newHeading + Math.PI * 2: newHeading;
            System.out.println(newHeading);
            headingList.add(newHeading);
        }

        for (int i = 0; i < headingList.size(); i++){
            list.get(i).setHeading(headingList.get(i));
        }
    }
}
