package ar.edu.itba.ss;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class CellIndexMethod {
	 public static HashMap<Integer, Set<Integer>> cellIndexMethod(final Space space, final Integer N, final Boolean noBorder){
	        final HashMap<Integer, Set<Integer>> hm = prepareHashMap(N);
	        final Integer M = space.getM();

	        for (int i = 0; i < M; i++){
	            for (int j = 0; j < M; j++){
	                HashSet<Particle> set = space.getParticlesXY(i, j);


					for (Particle particle : set){
	                    checkNeighborProximity(particle, i, j, space, hm, noBorder);
	                }
	                checkNeighborCell(set, hm, space.getRc(), space.getL());
	            }
	        }

	        return hm;
	    }

	    public static void checkNeighborProximity(final Particle particle, final int cellX, final int cellY, final Space space, final HashMap<Integer, Set<Integer>> hm, final Boolean noBorder){

	        //TODO: There some cases that do not require mod
	        final int M = space.getM();
	        final Double Rc = space.getRc();

	        boolean useTop = true;
	        boolean useTopRight = true;
	        boolean useRight = true;
	        boolean useDownRight = true;

	        if (!noBorder) {
	            useTop = !cellOnOtherBorder(cellX, cellY - 1, M);
	            useTopRight = !cellOnOtherBorder(cellX + 1, cellY - 1, M);
	            useRight = !cellOnOtherBorder(cellX + 1, cellY, M);
	            useDownRight = !cellOnOtherBorder(cellX + 1, cellY + 1, M);
	        }

	        //Top
	        final HashSet<Particle> topCell = space.getParticlesXY(mod(cellX, M), mod(cellY - 1, M));

	        //Top Right
	        final HashSet<Particle> topRightCell = space.getParticlesXY(mod(cellX + 1, M), mod(cellY - 1, M));

	        //Right
	        final HashSet<Particle> rightCell = space.getParticlesXY(mod(cellX + 1, M), mod(cellY, M));

	        //Down Right
	        final HashSet<Particle> downRightCell = space.getParticlesXY(mod(cellX + 1, M), mod(cellY + 1, M));


	        //Top
	        if (useTop) {
	            //topCell.stream().filter(p -> particleDist(particle, p, space.getL()) <= Rc).forEach(p -> {
	            //    hm.get(particle.getId()).add(p.getId());
	            //    hm.get(p.getId()).add(particle.getId());
	            //});
                for (Particle p : topCell) {
                    if (particleDist(particle, p, space.getL()) <=  Rc){
                        hm.get(particle.getId()).add(p.getId());
                        hm.get(p.getId()).add(particle.getId());
                    }
                }

	        }

	        //Top Right
	        if (useTopRight) {
	            //topRightCell.stream().filter(p -> particleDist(particle, p, space.getL()) <= Rc).forEach(p -> {
	            //    hm.get(particle.getId()).add(p.getId());
	            //    hm.get(p.getId()).add(particle.getId());
	            //});

                for (Particle p : topRightCell) {
                    if (particleDist(particle, p, space.getL()) <=  Rc){
                        hm.get(particle.getId()).add(p.getId());
                        hm.get(p.getId()).add(particle.getId());
                    }
                }
	        }

	        //Right
	        if (useRight) {
	            //rightCell.stream().filter(p -> particleDist(particle, p, space.getL()) <= Rc).forEach(p -> {
	            //    hm.get(particle.getId()).add(p.getId());
	            //    hm.get(p.getId()).add(particle.getId());
	            //});

                for (Particle p : rightCell) {
                    if (particleDist(particle, p, space.getL()) <=  Rc){
                        hm.get(particle.getId()).add(p.getId());
                        hm.get(p.getId()).add(particle.getId());
                    }
                }
	        }

	        //Down Right
	        if (useDownRight) {
	            //downRightCell.stream().filter(p -> particleDist(particle, p, space.getL()) <= Rc).forEach(p -> {
	            //    hm.get(particle.getId()).add(p.getId());
	            //    hm.get(p.getId()).add(particle.getId());
	            //});
                for (Particle p : downRightCell) {
                    if (particleDist(particle, p, space.getL()) <=  Rc){
                        hm.get(particle.getId()).add(p.getId());
                        hm.get(p.getId()).add(particle.getId());
                    }
                }
	        }

	    }

	    /**
	     *  Returns boolean indicating whether you have to consider
	     *  the cell or not, in case the border is being considered
	     *  in the space.
	     *
	     */
	    private static boolean cellOnOtherBorder(int x, int y, int M) {
	        return (x < 0 || y < 0 || x >= M || y >= M);
	    }

	    private static int mod(int c, int M) {
	        return c % M < 0 ? (c % M) + M : c % M;
	    }

	    private static void checkNeighborCell(HashSet<Particle> set, HashMap<Integer, Set<Integer>> hm, Double Rc, final Double L) {
            ArrayList<Particle> list = new ArrayList<>(set);

	        for (int i = 0; i < list.size(); i++) {
	            for (int j = i + 1; j < list.size(); j++) {
	                Particle part1 = list.get(i);
	                Particle part2 = list.get(j);

	                if (particleDist(part1, part2, L) < Rc) {
	                    hm.get(part1.getId()).add(part2.getId());
	                    hm.get(part2.getId()).add(part1.getId());
	                }
	            }
	        }
	    }

	    private static double particleDist(final Particle part1, final Particle part2, final Double L){
	        double  x1, x2, y1, y2;

	        if (part1.getX_pos() > part2.getX_pos()){
	            x2 = part2.getX_pos();
	            x1 = correctSpaceCoord(part1.getX_pos(), x2, L);
	        } else {
	            x1 = part1.getX_pos();
	            x2 = correctSpaceCoord(part2.getX_pos(), x1, L);
	        }

	        if (part1.getY_pos() > part2.getY_pos()){
	            y2 = part2.getY_pos();
	            y1 = correctSpaceCoord(part1.getY_pos(), y2, L);
	        } else {
	            y1 = part1.getY_pos();
	            y2 = correctSpaceCoord(part2.getY_pos(), y1, L);
	        }

	        return Math.sqrt(Math.pow(x2-x1, 2) + Math.pow(y2-y1, 2));// - part1.getRadius() - part2.getRadius();
	    }

	    private static double correctSpaceCoord(final Double c2, final double c1, final double L){
	        double candidate1 = c2;
	        double candidate2 = c2 - L;

	        double dist1 = Math.abs(candidate1-c1);
	        double dist2 = Math.abs(candidate2-c1);

	        if (dist1 < dist2)
	            return candidate1;
	        return candidate2;
	    }

	    private static HashMap<Integer, Set<Integer>> prepareHashMap(Integer N) {
	        final HashMap<Integer, Set<Integer>> hm = new HashMap<>();

	        for (int i = 0; i < N; i++){
	            hm.put(i, new HashSet<>());
	        }

	        return hm;
	    }
}
