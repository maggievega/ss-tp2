package ar.edu.itba.ss;


import java.util.ArrayList;
import java.util.Random;

public class Particle {

    private Integer id;

    private Double radius = 0.0;
    private Double property = 0.0;

    private Double x_pos = 0.0;
    private Double y_pos = 0.0;

    //private Double x_speed = 0.0;
    //private Double y_speed = 0.0;

    private Double speed = 0.0;
    private Double heading = 0.0;

    //TODO: Implement factory
    public Particle(final Integer id){
        this.id = id;
    }

    public Particle(double radius, double property){
        this.radius = radius;
        this.property = property;
    }

    public Particle(int id, double radius, double property, double x, double y, double speedModule, double heading){
        this.id = id;
        this.radius = radius;
        this.property = property;
        this.x_pos = x;
        this.y_pos = y;
        this.speed = speedModule;
        this.heading = heading;
    }

    public static ArrayList<Particle> makeParticles(int N, double L, double radius, double speedModule){
        ArrayList<Particle> list = new ArrayList<>();

        Random r = new Random();
        //r.setSeed(System.currentTimeMillis());

        for (int i = 0; i < N; i++) {
            double randomX = L * r.nextDouble();
            double randomY = L * r.nextDouble();

            double heading = 2 * Math.PI * r.nextDouble();

            list.add(new Particle(i, radius, 1, randomX, randomY, speedModule, heading));
        }

        return list;
    }

    public Double getHeading() {
        return heading;
    }

    public double getXSpeed(){
        return speed * Math.cos(heading);
    }

    public double getYSpeed(){
        return speed * Math.sin(heading);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Particle particle = (Particle) o;

        return id.equals(particle.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public Double getY_pos() {
        return y_pos;
    }

    public void setY_pos(double y_pos) {
        this.y_pos = y_pos;
    }

    public Double getX_pos() {
        return x_pos;
    }

    public void setX_pos(double x_pos) {
        this.x_pos = x_pos;
    }

    public Double getProperty() {
        return property;
    }

    public void setProperty(double property) {
        this.property = property;
    }

    public Double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public String toString() {
        return "Particle " + this.id + " (" + this.x_pos + "," + this.y_pos + ")";
    }

    public Integer getId() {
        return id;
    }

    public void setHeading(double heading) {
        this.heading = heading;
    }
}

